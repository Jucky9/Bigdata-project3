import pandas as pd
import vectorization as vect
from sklearn.naive_bayes import MultinomialNB
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import KFold
from sklearn.model_selection import train_test_split
import numpy as np

MNB_values=[]
SVC_values=[]
DT_values=[]
LR_values=[]


data = pd.read_csv('labelised_15kW_12kB.csv')
label = data["malicious"]
training_data, testing_data = train_test_split(data, test_size = 0.4)

y_train = training_data['malicious']
y_test = testing_data['malicious']

print(training_data.shape)
count = pd.value_counts(training_data['malicious'], sort = True)
print(count)
print("")
print("% of whitelisted cases in training dataset =",(count[0]/(count[0]+count[1])) * 100)
print("")

training_data_temp =[training_data['q_name']]
testing_data_temp =[testing_data['q_name']]

x_train, x_test = vect.vectorization(training_data_temp,testing_data_temp)

x_train = x_train.fillna(0)
y_train = y_train.fillna(0)
x_test = x_test.fillna(0)
y_test = y_test.fillna(0)

############################################

clf = LinearSVC(max_iter=12000)
clf.fit(x_train, y_train)
predictionSVC = clf.predict(x_test)

print("SVC: "+str(accuracy_score(y_test,predictionSVC)*100))

SVC_values.append(accuracy_score(y_test,predictionSVC)*100)

if_cm=confusion_matrix(predictionSVC,y_test)
df_cm = pd.DataFrame(if_cm,['True Black','True White'],['Pred Black','Pred White'])
print(df_cm)

print("")

##############################################

tree = DecisionTreeClassifier()
tree.fit(x_train, y_train)
predicttionsDecisionTree = tree.predict(x_test)
print("decisionTree: "+str(accuracy_score(y_test,predicttionsDecisionTree)*100))

DT_values.append(accuracy_score(y_test,predicttionsDecisionTree)*100)

if_cm=confusion_matrix(predicttionsDecisionTree,y_test)
df_cm = pd.DataFrame(if_cm,['True Black','True White'],['Pred Black','Pred White'])
print(df_cm)

print("")

##############################################

log = LogisticRegression(solver='lbfgs',max_iter=8000)
log.fit(x_train, y_train)
predictionLog = log.predict(x_test)

print("LogisticRegression: "+str(accuracy_score(y_test,predictionLog)*100))

LR_values.append(accuracy_score(y_test,predictionLog)*100)

if_cm=confusion_matrix(predictionLog,y_test)
df_cm = pd.DataFrame(if_cm,['True Black','True White'],['Pred Black','Pred White'])
print(df_cm)

print("")



total_SVC_score=0
for score in SVC_values:
	total_SVC_score+=score
final_average_SVC = total_SVC_score/len(SVC_values)
print("SVC average: "+str(final_average_SVC))

total_DT_score=0
for score in DT_values:
	total_DT_score+=score
final_average_DT = total_DT_score/len(DT_values)
print("DT average: "+str(final_average_DT))

total_LR_score=0
for score in LR_values:
	total_LR_score+=score
final_average_LR = total_LR_score/len(LR_values)
print("LR average: "+str(final_average_LR))


